require_relative '../lib/customer'
require_relative '../lib/product'
require_relative '../lib/order'

RSpec.describe Order do
  describe '#total_amount' do
    it 'returns the total amount' do
      customer = Customer.new
      product = Product.new(name: '1984 - George Orwell', type: :book)
      order = Order.new(customer)
      order.add_product(product)

      expect(order.total_amount).to eq(10)
    end

    it 'returns the total amount with two items' do
      customer = Customer.new
      product = Product.new(name: '1984 - George Orwell', type: :book)
      order = Order.new(customer)
      order.add_product(product)
      order.add_product(product)

      expect(order.total_amount).to eq(20)
    end
  end

  describe '#add_product' do
    it 'adds products' do
      customer = Customer.new
      product = Product.new(name: '1984 - George Orwell', type: :book)
      order = Order.new(customer)
      order.add_product(product)

      expect(order.items).not_to be_empty
    end
  end

  describe '#deliver' do
    it 'generates a shipping label to a physical item' do
      customer = double('customer')
      address = double('address', to_s: '18120-000')
      product = double('product', type: :physical)
      order = Order.new(customer, address: address)
      order.add_product(product)

      order.deliver

      expect(order.shipping_label).to eq("Ship to: 18120-000")
    end

    it 'not generates a shipping label to a digital item' do
      customer = double('customer').as_null_object
      address = double('address', to_s: '18120-000')
      product = double('product', type: :digital)
      order = Order.new(customer)
      order.add_product(product)

      order.deliver

      expect(order.shipping_label).to be_nil
    end

    it 'activate a membership if the product is a membership' do
      customer = spy('customer')
      product = double('product', type: :membership)
      order = Order.new(customer)
      order.add_product(product)

      order.deliver

      expect(customer).to have_received(:activate_membership)
    end

    it 'send an email to the customer if the product is a membership' do
      customer = spy('customer')
      product = double('product', type: :membership)
      order = Order.new(customer)
      order.add_product(product)

      order.deliver

      expect(customer).to have_received(:send_email)
    end

    it 'generates a shipping label to a book item' do
      customer = double('customer')
      address = double('address', to_s: '18120-000')
      product = double('product', type: :book)
      order = Order.new(customer, address: address)
      order.add_product(product)

      order.deliver

      expected_text = "Ship to: 18120-000
Item isento de impostos conforme disposto na Constituição Art. 150, VI, d."

      expect(order.shipping_label).to eq(expected_text)
    end

    it 'send an email to the customer if the product is digital' do
      customer = spy('customer')
      address = double('address', to_s: '18120-000')
      product = double('product', type: :digital)
      order = Order.new(customer, address: address)
      order.add_product(product)

      order.deliver

      expect(customer).to have_received(:send_email)
    end

    it 'activate a membership if the product is a membership' do
      customer = spy('customer')
      address = double('address', to_s: '18120-000')
      product = double('product', type: :digital)
      order = Order.new(customer, address: address)
      order.add_product(product)

      order.deliver

      expect(customer).to have_received(:grant_voucher).with(10)
    end
  end
end
