require_relative '../lib/customer'
require_relative '../lib/product'
require_relative '../lib/order'
require_relative '../lib/credit_card'
require_relative '../lib/payment'

RSpec.describe Payment do
  describe '#pay' do
    it 'closes the order' do
      order = spy('order')
      time = Time.now

      payment = Payment.new(order: order)
      payment.pay(time)

      expect(order).to have_received(:close).with(time)
    end

    it 'deliver the items' do
      order = spy('order')
      payment = Payment.new(order: order)
      payment.pay

      expect(order).to have_received(:deliver)
    end
  end
end
