# Considerações sobre o desafio

Eu fiz o desafio usando Ruby com RSpec por ser o ambiente que eu mais conheço. Eu comecei criando alguns testes simples para as classes Payment e Order por serem as mais importantes no sistema. Depois eu comecei implementando todas as regras de entregar do produtos na classe Order e por último transformei o switch statement em uma factory que retorna a classe específica para cada tipo de produto.

## Como executar os testes

Primeiro é necessário instalar as dependências do projeto.

```
bundle install
```

Depois é só executar o RSpec.

```
rspec spec
```

Muito obrigado pela oportunidade e estou disponível para dúvidas e esclarecimentos.
