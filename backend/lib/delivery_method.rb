require_relative './delivery_methods/physical'
require_relative './delivery_methods/membership'
require_relative './delivery_methods/book'
require_relative './delivery_methods/digital'

class DeliveryMethod
  def self.for(product_type, order)
    begin
      const_get("DeliveryMethods::#{product_type.capitalize}")
    end.new(order)
  end
end
