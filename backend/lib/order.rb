require_relative './address'
require_relative './order_item'
require_relative './delivery_method'

class Order
  attr_reader :customer, :items, :payment, :address, :closed_at
  attr_accessor :shipping_label

  def initialize(customer, overrides = {})
    @customer = customer
    @items = []
    @order_item_class = overrides.fetch(:item_class) { OrderItem }
    @address = overrides.fetch(:address) { Address.new(zipcode: '45678-979') }
  end

  def add_product(product)
    @items << @order_item_class.new(order: self, product: product)
  end

  def total_amount
    @items.map(&:total).inject(:+)
  end

  def close(closed_at = Time.now)
    @closed_at = closed_at
  end

  def deliver
    @items.each do |order_item|
      method = DeliveryMethod.for(order_item.product.type, self)
      method.apply
    end
  end

  # remember: you can create new methods inside those classes to help you create a better design
end
