class Customer
  def activate_membership
    p "Membership acitive"
  end

  def send_email
    p "Sending email..."
  end

  def grant_voucher(value)
    p "Received a new voucher: #{value}"
  end
end
