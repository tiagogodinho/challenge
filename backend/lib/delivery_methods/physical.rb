module DeliveryMethods
  class Physical
    def initialize(order)
      @order = order
    end

    def apply
      @order.shipping_label = "Ship to: #{@order.address.to_s}"
    end
  end
end
