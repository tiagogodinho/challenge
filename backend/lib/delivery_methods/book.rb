module DeliveryMethods
  class Book
    def initialize(order)
      @order = order
    end

    def apply
      @order.shipping_label = "Ship to: #{@order.address.to_s}"
      @order.shipping_label += "\nItem isento de impostos conforme disposto na Constituição Art. 150, VI, d."
    end
  end
end
