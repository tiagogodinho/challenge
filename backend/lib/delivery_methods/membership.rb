module DeliveryMethods
  class Membership
    def initialize(order)
      @order = order
      @customer = @order.customer
    end

    def apply
      @customer.activate_membership
      @customer.send_email
    end
  end
end
