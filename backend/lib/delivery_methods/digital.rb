module DeliveryMethods
  class Digital
    def initialize(order)
      @order = order
      @customer = @order.customer
    end

    def apply
      @customer.grant_voucher(10)
      @customer.send_email
    end
  end
end
